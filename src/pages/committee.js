import Container from '@material-ui/core/Container';
import Table from '@material-ui/core/Table';
import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import styles from "./index.module.css";

const SecondPage = () => (
  <Layout title="The Committee">
    <SEO title="Contact Us" />

    {/* <Container className={styles.section}>
      <h1 className={styles.h1}>Swarms</h1>
      <p>
        Please see our dedicated <Link to="/swarms">swarm</Link> page
      </p>
    </Container> */}
    <Container className={styles.section}>
      <h1 className={styles.h1}>Committee Members</h1>
      <Table hover size="sm">
        <thead>
          <tr>
            <th>Name</th>
            <th>Position</th>
            <th>Location</th>
            <th>Phone</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Rod Saffery</td>
            <td>President</td>
            <td>Totnes</td>
            <td>01803 472656</td>
            <td>rodsaffery@gmail.com</td>
          </tr>
          <tr>
            <td>Tammy Skinner</td>
            <td>Chairperson</td>
            <td>Totnes</td>
            <td>01803 868947</td>
            <td>tamskinner@icloud.com</td>
          </tr>
          <tr>
            <td>Graham Brown</td>
            <td>Vice-Chairperson</td>
            <td>Buckfastleigh</td>
            <td>01364 642787</td>
            <td>graham.brown@gmx.us</td>
          </tr>
          <tr>
            <td>Lilah Killock</td>
            <td>Secretary</td>
            <td>Totnes</td>
            <td>01803 866028</td>
            <td>lilahkillock@btinternet.com</td>
          </tr>
          <tr>
            <td>Lilah Killock</td>
            <td>Apiary Manager</td>
            <td>Totnes</td>
            <td>01803 866028</td>
            <td>lilahkillock@btinternet.com</td>
          </tr>
          <tr>
            <td>Annette Quartly</td>
            <td>Treasurer</td>
            <td>Plymstock</td>
            <td>01752 492471</td>
            <td>annette.quartly@cantab.net</td>
          </tr>
          <tr>
            <td>Kevin Gillard</td>
            <td>Education</td>
            <td>Kingsbridge</td>
            <td>01548 810148</td>
            <td>fonso@tiscali.co.uk</td>
          </tr>
          <tr>
            <td>Byron Williams</td>
            <td>Website</td>
            <td>Totnes</td>
            <td>077 48 128 709</td>
            <td>byron@112percent.com (preferred)</td>
          </tr>
          <tr>
            <td>Karen Allen</td>
            <td></td>
            <td>Totnes</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Kathy Lovegrove</td>
            <td></td>
            <td>Plymouth</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Jeremy Wells</td>
            <td></td>
            <td>South Brent</td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </Table>
    </Container>
    <Container className={styles.section}>
      <h1 className={styles.h1}>Useful Contacts</h1>
      <Table hover size="sm">
        <thead>
          <tr>
            <th>Name</th>
            <th>Position</th>
            <th>Location</th>
            <th>Phone</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Jo Lapthorne</td>
            <td>Asian Hornet Coordinator</td>
            <td>Kingsbridge</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>WhatsApp Group</td>
            <td>Asian Hornet Group</td>
            <td colspan="3">
              <a href="https://chat.whatsapp.com/IJGKpBt8IWvA8qpdJLpwff">
                https://chat.whatsapp.com/IJGKpBt8IWvA8qpdJLpwff
              </a>
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  </Layout>
)

export default SecondPage
