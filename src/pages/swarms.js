import Container from "@material-ui/core/Container"
import Grid from "@material-ui/core/Grid"
import React from "react"
import ImageCard from "../components/imagecard"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styles from "./index.module.css"

const SecondPage = () => (
  <Layout title="Swarm Collection">
    <SEO title="Swarm Collection" />

    <Container className={styles.section}>
      <h1 className={styles.h1}>Swarms</h1>
      <p>
        Has a honey bee swarm decided to make a home in your roof, compost bin,
        or is it just waiting to be collected on a fence post?
      </p>
      <p>Swarms look like these:</p>
      <Grid container spacing={5}>
        <Grid item xs={12} sm={6} md={4} spacing={1}>
          <ImageCard
            title="Swarm on a fence post"
            imageSrc="https://byron-buzz.imgix.net/IMG-20190507-WA0000.jpg?h=250"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4} spacing={1}>
          <ImageCard
            title="Swarm under an eave"
            imageSrc="https://tkbka.imgix.net/swarm-1.jpg?h=250&auto=format&fit=crop&crop=focalpoint"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4} spacing={1}>
          <ImageCard
            title="Swarm on a lamp post"
            imageSrc="https://tkbka.imgix.net/swarm-2.jpg?h=250&auto=format&fit=crop&crop=focalpoint"
          />
        </Grid>
      </Grid>
      <p>Swarms are not:</p>
      <ul>
        <li>
          A few bees here and there... there will be a lot of them, around,
          20,000
        </li>
        <li>Bumblebees, wasps, flies or hornets</li>
      </ul>
      <p>Areas covered</p>
      <ul>
        <li>Buckfastleigh to Totnes</li>
        <li>Dartmouth to Salcombe</li>
        <li>Kingsbridge to Ivybridge</li>
        <li>... and everywhere in the middle</li>
      </ul>
      <p>
        We have a dedicated number you can phone 9am - 6pm throughout April -
        September.
      </p>
      <ul>
        <li>07311 471 887</li>
      </ul>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </Container>
  </Layout>
)

export default SecondPage
