import Container from "@material-ui/core/Container"
import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styles from "./index.module.css"

const SecondPage = () => (
  <Layout title="Courses">
    <SEO title="Courses" />

    <Container className={styles.section}>
      <h1 className={styles.h1}>Beginners Course</h1>

      <p>
        We run a beginners course beginning every January. Highlights include:
      </p>
      <ul>
        <li>
          Theory: What a honey bee is, what is does, beekeeping through the
          seasons
        </li>
        <li>
          Equipment: Different types of hives, putting them together, harvesting
          honey
        </li>
        <li>Practical: Handling bees and the equipment with confidence</li>
      </ul>
      <p>
        Practical sessions at the branch apiary, where, guided by an experience
        beekeeper, you will learn how to handle the bees and equipment with care
        and confidence. For more information and dates and times of this years
        course click here. To book your place on the course email Lilah by
        clicking here.
      </p>
      <h3>Course Dates 2020</h3>
      <p>
        <strong>Theory</strong>
      </p>
      <ul>
        <li>9th Jan: Introduction</li>
        <li>23rd Jan: The Colony</li>
        <li>6th Feb: The Hive</li>
        <li>20th Feb: Queens</li>
        <li>5th Mar: Swarms, etc...</li>
        <li>19th Mar: Pests and Diseases</li>
        <li>2nd Apr: The Beekeeping Year</li>
        <li>16th Apr: Bees, Plants and Hive Products</li>
      </ul>
      <p>
        <strong>Practical</strong>
      </p>
      <ul>
        <li>From May, dates dependent upon weather</li>
      </ul>
    </Container>
  </Layout>
)

export default SecondPage
