import Container from "@material-ui/core/Container"
import Hidden from "@material-ui/core/Hidden"
import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styles from "./index.module.css"
import Button from "@material-ui/core/Button"
import { createStyles, makeStyles } from "@material-ui/core/styles"
import GridList from "@material-ui/core/GridList"
import GridListTile from "@material-ui/core/GridListTile"
import GridListTileBar from "@material-ui/core/GridListTileBar"

const fmt = new Intl.NumberFormat("en-GB", { maximumFractionDigits: 0 })

const members = 128
// const membersWithHives = 101
const branchApiaryHives = 6
const hives =
  (75 * 3 + 5 * 10 + 15 * 10 + 2 * 25 + 1 * 30) * 0.8 + branchApiaryHives

// http://scientificbeekeeping.com/understanding-colony-buildup-and-decline-part-10/
// https://mycurvefit.com/
// Quartic fit
// 1                8272
// 2                9872
// 3               17283
// 4               22383
// 5               38628
// 6               55000
// 7               48291
// 8               40000
// 9               30000
// 12                9872

function calcBees() {
  const now = new Date()
  const x =
    now.getMonth() +
    now.getDate() / 30 +
    now.getHours() / 24 / 30 +
    now.getMinutes() / 60 / 24 / 30 +
    now.getSeconds() / 60 / 60 / 24 / 30
  const y =
    6063.952 -
    10857.95 * x +
    9450.642 * Math.pow(x, 2) -
    1446.805 * Math.pow(x, 3) +
    61.27418 * Math.pow(x, 4)
  const bees = hives * y
  return bees
}

// class MyCard extends React.PureComponent {
//   render() {
//     return (
//       <Card>
//         <CardContent>
//           <Typography variant="h4">{this.props.value}</Typography>
//           <Typography variant="h6">{this.props.title}</Typography>
//         </CardContent>
//       </Card>
//     )
//   }
// }

const tileData = [
  {
    img:
      "https://tkbka.imgix.net/IMG_20190522_161734.jpg?h=250&auto=format&fit=crop&crop=focalpoint",
    title: "Frame of Bees",
  },

  {
    title: "Fully capped frame of brood",
    img:
      "https://tkbka.imgix.net/IMG_20190714_101944.jpg?w=250&auto=format&fit=crop&crop=focalpoint",
  },
  {
    title: "Beeswax Candles",
    img:
      "https://tkbka.imgix.net/IMG_20191025_140640.jpg?h=250&auto=format&fit=crop&crop=focalpoint",
  },
  {
    title: "Queen on a Frame",
    img:
      "https://tkbka.imgix.net/IMG_3915.JPG?h=250&auto=format&fit=crop&crop=focalpoint",
  },
  {
    title: "Teaching",
    img:
      "https://tkbka.imgix.net/IMG_20190522_175527.jpg?w=250&auto=format&fit=crop&crop=focalpoint",
  },
  {
    title: "Jars of Honey",
    img:
      "https://tkbka.imgix.net/IMG_20190831_142835.jpg?h=250&auto=format&fit=crop&crop=focalpoint",
  },
]

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
    },
    gridList: {
      width: "100%",
    },
    icon: {
      color: "rgba(255, 255, 255, 0.54)",
    },
  })
)

export default function TitlebarGridList() {
  const classes = useStyles()

  // state = {
  //   bees: 0,
  //   delay: 5000,
  // }

  // componentDidMount() {
  //   this.setState({ bees: calcBees() })
  //   this.interval = setInterval(this.tick, this.state.delay)
  // }

  // tick = () => {
  //   this.setState({ bees: calcBees() })
  // }

  // componentWillUnmount() {
  //   clearInterval(this.interval)
  // }

  return (
    <Layout title="Welcome">
      <SEO title="Home" />
      {/* <Grid container spacing={1}>
            <Grid item xs={12} md={3} spacing={3}>
              <MyCard title="Members" value={fmt.format(members)} />
            </Grid>
            <Grid item xs={12} md={3} spacing={3}>
              <MyCard title="Hives" value={fmt.format(hives)} />
            </Grid>
            <Grid item xs={12} md={3} spacing={3}>
              <MyCard title="Bees" value={"~" + fmt.format(this.state.bees)} />
            </Grid>
            <Grid item xs={12} md={3} spacing={3}>
              <MyCard title="Yearly shows attended" value={5} />
            </Grid>
          </Grid> */}

      <Container>
        <p>
          If you're a beekeeper or an aspiring beekeeper then we'd love you to
          get involved with our local branch of the Devon Beekeeping
          Association. We represent beekeepers in the South Hams area from
          Totnes down to Kingsbridge and the surrounding area
        </p>
        <p>
          We host the annual South Devon Beekeeping Convention and attend many
          local events. Come along and see us.
        </p>
        <p>&nbsp;</p>
        <hr />
        <h2>Wildflower verges and hedges for Devon Petition</h2>
        <p>
          The 38 Degrees petition is asking Devon County Council to stop mowing
          the verges and flailing the hedges
        </p>
        <p>
          If successful it will help ensure that Devon County Council will treat
          all Devon hedges, banks and verges as wildlife and wild plant havens
          and that they are cared for as a precious environmental resource.
        </p>
        <Button
          variant="contained"
          color="primary"
          target="_blank"
          href="https://you.38degrees.org.uk/petitions/wildflower-verges-and-hedges-for-devon?bucket&fbclid=IwAR2IyrePxP4yp9MJhDnE-TRMJR4nmeNGZH7ShVtphIYT_sLpucOqKLB-y1A"
        >
          Sign Petition
        </Button>
        <hr />
        <p>&nbsp;</p>

        <GridList cellHeight={250} cols={3} className={classes.gridList}>
          {tileData.map(tile => (
            <GridListTile key={tile.img}>
              <img src={tile.img} alt={tile.title} />
              <GridListTileBar title={tile.title} />
            </GridListTile>
          ))}
        </GridList>
      </Container>
      <Container className={styles.section}>
        <h1 className={styles.h1} id="events">
          Events
        </h1>
        <iframe
          src="https://calendar.google.com/calendar/embed?src=q7m0g1s1hk4e63htcun30fa37c%40group.calendar.google.com&ctz=Europe%2FLondon&wkst=2&mode=AGENDA"
          style={{ border: 0 }}
          width="100%"
          height="300"
          frameborder="0"
          scrolling="no"
        ></iframe>
        <p>
          If you have an event you would like to add, please contact the
          secretary
        </p>
      </Container>
    </Layout>
  )
}
