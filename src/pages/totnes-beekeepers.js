import Container from '@material-ui/core/Container'
import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"



const SecondPage = () => (
  <Layout>
    <SEO title="Totnes Beekeepers" />

    <Container>
      <h1>Totnes Beekeepers</h1>
      <p>
        Fancy joining a beekeeping club near Totnes? Totnes and Kingsbridge
        Beekeepers may be for you. Whether you are an aspiring beekeeper or
        you are just moving to the area come and meet us.
      </p>
    </Container>
  </Layout>
)

export default SecondPage
