import { Divider } from "@material-ui/core"
import Card from "@material-ui/core/Card"
import CardActionArea from "@material-ui/core/CardActionArea"
import CardContent from "@material-ui/core/CardContent"
import CardMedia from "@material-ui/core/CardMedia"
import Typography from "@material-ui/core/Typography"
import { styled } from '@material-ui/styles';
import React from "react"

const MyCard = styled(Card)({
  maxWidth: "345px",
  width:"95%"
})

const MyCardMedia = styled(CardMedia)({
  height: "250px",
  maxHeight: "216px"
})

export default function ImageCard({ title, imageSrc }) {
  return (
    <MyCard>
      <CardActionArea>
        <MyCardMedia image={imageSrc} title={title} />
        <Divider />
        <CardContent>
          <Typography gutterBottom component="p">
            {title}
          </Typography>
          {/* <Typography variant="body2" color="textSecondary" component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>*/}
        </CardContent>
      </CardActionArea>
      {/* <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions> */}
    </MyCard>
  )
}
